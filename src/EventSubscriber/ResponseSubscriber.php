<?php

namespace Drupal\jsonapi_gutenberg\EventSubscriber;

use Drupal\book\BookManagerInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\gutenberg\Parser\BlockParser;
use Drupal\jsonapi\Routing\Routes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber which alters JSON:API responses for book related nodes.
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * An array with already loaded book definitions.
   *
   * @var array
   */
  protected array $loadedBookDefinitions = [];

  /**
   * An array of already laaded UUIDs.
   *
   * @var array
   */
  protected array $loadedUUIDs = [];

  /**
   * An array of already laaded Urls.
   *
   * @var array
   */
  protected array $loadedUrls = [];

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|null
   */
  protected ?EntityStorageInterface $nodeStorage = NULL;

  /**
   * ResponseSubscriber constructor.
   *
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    try {
      $this->nodeStorage = $entityTypeManager->getStorage('node');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      // This is just theoretical and should never happen.
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE] = ['onResponse'];
    return $events;
  }

  /**
   * Set route match service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The  route match service.
   */
  public function setRouteMatch(RouteMatchInterface $route_match): void {
    $this->routeMatch = $route_match;
  }

  /**
   * Called when KernelEvents::RESPONSE event is dispatched.
   *
   * If the response contains content from the JSON:API, it checks if any of the
   * records are nodes to find optional book definitions for those nodes adding
   * them as the new attribute 'drupal_internal__book' to each applicable
   * record.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The filter event.
   *
   * @throws \JsonException
   */
  public function onResponse(ResponseEvent $event): void {
    if ($this->nodeStorage === NULL || !$this->routeMatch->getRouteObject()) {
      return;
    }
    if ($this->routeMatch->getRouteName() === 'jsonapi.resource_list' || Routes::isJsonApiRequest($this->routeMatch->getRouteObject()
        ->getDefaults())) {
      $response = $event->getResponse();
      $content = $response->getContent();
      $jsonapi_response = json_decode($content, TRUE, 512, JSON_THROW_ON_ERROR);
      if (!is_array($jsonapi_response)) {
        return;
      }
      if (isset($jsonapi_response['data']['attributes']['drupal_internal__nid'])) {
          // The response contains one distinct node.
        $jsonapi_response['data']['attributes']['body']['blocks'] = $this->parseBlocks($jsonapi_response['data']);
      }
      elseif (isset($jsonapi_response['data'][0]['attributes']['drupal_internal__nid'])) {
        // The response contains a list of nodes.
        foreach ($jsonapi_response['data'] as $key => $values) {
          $jsonapi_response['data'][$key]['attributes']['body']['blocks'] = $this->parseBlocks($values);
        }
      }
      $response->setContent(json_encode($jsonapi_response, JSON_THROW_ON_ERROR));
    }
  }

  protected function parseBlocks(array $record) {
    $bodyValue = $record['attributes']['body']['value'];
    $block_parser = new BlockParser();
    return $block_parser->parse($bodyValue);
  }

}
